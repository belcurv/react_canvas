export { default as collides }     from './collides';
export { default as createMatrix } from './createMatrix';
export { default as findFirstRow } from './findFirstRow';
export { default as merge }        from './merge';
export { default as rotate }       from './rotate';
export { default as resetPlayer }  from './resetPlayer';
